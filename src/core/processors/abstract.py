import time

import cv2


class AbstractProcessor(object):

    def __init__(self, config):
        self.config = config
        self.algorithms_impls = {}
        self.processor_types = []

    def get_config(self):
        if self.get_processor_name() in self.config:
            return self.config[self.get_processor_name()]
        else:
            return None

    def get_processor_name(self):
        return 'ABSTRACT'

    def resize_img_if_needed(self, image, algorithm_name):
        config = self.get_config()[algorithm_name]

        if 'resize_to' not in config:
            return image

        weight = config['resize_to']['weight']
        height = config['resize_to']['height']

        return cv2.resize(image, (weight, height))

    def process(self, context):
        if self.get_config() is None:
            return None, None

        print('Processor: {}'.format(self.get_processor_name()))
        image = context.get_cur_image()
        i = context.get_cur_image_idx()

        processor_result = {}
        for algorithm_name in self.get_config().keys():
            print('Processing image {}/{} with algorithm: {}'.format(i + 1,
                                                                     context.get_images_count(),
                                                                     algorithm_name))
            if algorithm_name not in self.algorithms_impls:
                print('No such algorithm in implementations: {}, skipping'.format(algorithm_name))
                continue

            start_alg_time = time.time()

            img = self.resize_img_if_needed(image, algorithm_name)

            single_img_result = self.algorithms_impls[algorithm_name](algorithm_name, img)

            end_alg_time = time.time()

            alg_time = end_alg_time - start_alg_time

            context.add_time(alg_time)

            print('Time of processing image={} with algorithm={} is {} seconds'.format(context.get_cur_image_idx(),
                                                                                       algorithm_name, alg_time))

            processor_result[algorithm_name] = single_img_result

        return processor_result, i
