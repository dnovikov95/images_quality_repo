import argparse
import json
import os
import shutil

if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument("-j", "--json", required=True,
                    help="path to json result file")
    ap.add_argument("-o", "--output", required=True, help="path to output images")
    ap.add_argument("-f", "--filter", required=True, help="filter: defocused/sharp")
    args = vars(ap.parse_args())

    output_path = args["output"]
    json_file_path = args["json"]
    filter = args["filter"]

    if not os.path.exists(output_path):
        os.mkdir(output_path)

    with open(json_file_path, 'r') as f:
        json_file_content = json.load(f)


    if (filter == 'defocused'):
        is_defocused = True
    elif(filter == 'sharp'):
        is_defocused = False
    else:
        exit(1)

    for unit in json_file_content:
        if unit['is_defocused'] == is_defocused:
            src_path = unit['image_path:']
            focus_measure = int(unit['focus_measure:'])
            shutil.copy(src_path, os.path.join(output_path, os.path.basename(src_path) + str(focus_measure) + '.png'))




