import json
import os

dataset_dir = '/home/dmitry/MIPT/Diplom/datasets/overexposure_custom'
output_markup_path = os.path.join(dataset_dir, 'markup.json')

with open(os.path.join(dataset_dir, 'images.lst'), 'r') as f:
    images_names = f.readlines()

res = {}

for img_name in images_names:
    img_name = img_name.strip()
    abs_img_path = os.path.abspath(os.path.join(dataset_dir, img_name))

    is_defected = True
    if 'normal' in abs_img_path:
        is_defected = False

    res[abs_img_path] = is_defected

with open(output_markup_path, 'w+') as f:
    json.dump(res, f)
