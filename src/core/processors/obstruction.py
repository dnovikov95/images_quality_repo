import cv2
import numpy as np

from src.core.processors.abstract import AbstractProcessor


class ObstructionProcessor(AbstractProcessor):
    def __init__(self, config):
        super(ObstructionProcessor, self).__init__(config)
        self.algorithms_impls = {}
        if self.get_config() is not None:
            for alg_name in self.get_config().keys():
                if alg_name.startswith("hist_based"):
                    self.algorithms_impls[alg_name] = self.hist_based_impl

    def get_processor_name(self):
        return "OBSTRUCTION"

    def hist_based_impl(self, algorithm_name, image):
        hist_based_config = self.get_config()[algorithm_name]
        gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        window_size = hist_based_config['window_size']
        threshold_percent = hist_based_config['percentage']

        hist = cv2.calcHist([gray_image], [0], None, [256], [0, 256])

        window_sums = []
        for i in range(0, 255 - window_size):
            window_sums.append(np.sum(hist[i: i + window_size]))

        obstruction_percent = int(np.max(window_sums) / hist.sum() * 100.)

        is_obstructed = True if obstruction_percent > threshold_percent else False

        result = {
            "result": is_obstructed,
            "percent": obstruction_percent,
            "value": obstruction_percent

        }

        return result
