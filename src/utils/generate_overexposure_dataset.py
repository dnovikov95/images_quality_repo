import os

import cv2
import numpy as np


def generate_img(img, n_stops):
    k = 2 ** n_stops
    img = img / 255.
    img = k * np.power(img, 2.2)
    img = np.power(img, 1 / 2.2)
    img[img > 1] = 1
    img = np.uint8(img * 255)
    return img


if __name__ == '__main__':
    src_images_path = '/home/dmitry/MIPT/Diplom/datasets/overexposure_custom/normal_old'
    dst_images_path = '/home/dmitry/MIPT/Diplom/datasets/overexposure_custom/normal'

    with open(os.path.join(src_images_path, 'images.lst'), 'r') as f:
        images_names = f.readlines()

    i = 1
    for img_name in images_names:
        print "Processing {}/{}".format(i, len(images_names))
        img_name = img_name.strip()
        img = cv2.imread(os.path.join(src_images_path, img_name.strip()))

        res_img = generate_img(img, 1)
        cv2.imwrite(os.path.join(dst_images_path, img_name), res_img)
        i += 1
