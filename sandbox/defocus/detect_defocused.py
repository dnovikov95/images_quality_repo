import argparse
import json
import os

import cv2


def is_defocused(image, threshold):
    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    focus_measure = cv2.Laplacian(gray_image, cv2.CV_64F).var()
    if focus_measure < threshold:
        return (True, focus_measure)
    return (False, focus_measure)


def process_images(images_path, output, threshold):
    result = []
    images_names = os.listdir(images_path)
    total_count = len(images_names)
    cur_image = 1
    defocused_count = 0

    for image_name in images_names:
        cur_image += 1
        print('Processing {}/{}'.format(cur_image, total_count))
        image_path = os.path.join(images_path, image_name)
        image = cv2.imread(image_path)
        is_def, focus_measure = is_defocused(image, threshold)

        if is_def:
            defocused_count += 1

        result.append({'image_path:': image_path, 'focus_measure:': focus_measure,
                       'is_defocused': is_def})

    json_result = json.dumps(result)
    with open(output, 'w+') as f:
        f.write(json_result)

    print('Total: {}, Defocused: {}, Sharp: {}'.format(total_count, defocused_count, total_count - defocused_count))


if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--images", required=True,
                    help="path to input directory of images")
    ap.add_argument("-t", "--threshold", type=float, default=100.0,
                    help="threshold")
    ap.add_argument("-o", "--output", required=True, help="path to output file")
    args = vars(ap.parse_args())

    images_path = args['images']
    threshold = args["threshold"]
    output = args["output"]

    process_images(images_path, output, threshold)
