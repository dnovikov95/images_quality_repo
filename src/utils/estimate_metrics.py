import argparse
import json

import numpy as np
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score


def prepare_data(true_path, predicted_path, processor_name):
    true_json = read_file(true_path)
    predicted_json = read_file(predicted_path)

    method_names = predicted_json[predicted_json.keys()[0]][processor_name].keys()

    y_true = []
    predicted = {}

    for method_name in method_names:
        predicted[method_name] = []

    for key in predicted_json:
        y_true.append(true_json[key])
        predicted_results = predicted_json[key][processor_name]
        for method_name in predicted_results:
            predicted[method_name].append(predicted_results[method_name]['result'])

    for key in predicted:
        predicted[key] = np.array(predicted[key])

    y_true = np.array(y_true)

    return y_true, predicted


def read_file(file_path):
    with open(file_path, 'r') as f:
        res = json.load(f)

    return res


if __name__ == '__main__':
    ap = argparse.ArgumentParser()

    ap.add_argument("-m", "--markup", required=True,
                    help="Path to markup json file")
    ap.add_argument("-p", "--predicted", required=True,
                    help="Path to images quality estimation result json file")
    ap.add_argument("-n", "--name", required=True,
                    help="Estimated processor name: [OVEREXPOSURE, DEFOCUS, OBSTRUCTION]")

    args = vars(ap.parse_args())

    true_path = args['markup']
    predicted_path = args['predicted']
    processor_name = args['name']

    y_true, predicted = prepare_data(true_path, predicted_path, processor_name)

    keys = predicted.keys()
    nums = []
    for k in keys:
        start, n = k.split('-')
        nums.append(int(n))

    for n in sorted(nums):
        method_name = start + '-' + str(n)
        print method_name
        y_pred = predicted[method_name]
        # print "Accuracy: {}".format(accuracy_score(y_true, y_pred))
        print "Precision: {}".format(precision_score(y_true, y_pred))
        print "Recall: {}".format(recall_score(y_true, y_pred))
        # print "F1 measure: {}".format(f1_score(y_true, y_pred))
        print

    # for method_name in sorted(predicted):
    #     print method_name
    #     y_pred = predicted[method_name]
    #     print "Accuracy: {}".format(accuracy_score(y_true, y_pred))
    #     print "Precision: {}".format(precision_score(y_true, y_pred))
    #     print "Recall: {}".format(recall_score(y_true, y_pred))
    #     print "F1 measure: {}".format(f1_score(y_true, y_pred))
    #     print
