import argparse
import json
import os

import cv2

from src.core.processors.defocus import DefocusProcessor
from src.core.processors.obstruction import ObstructionProcessor
from src.core.processors.overexposure import OverexposureProcessor


class ImagesContext(object):

    def __init__(self, images_path):
        self.images_paths = images_path
        self.result = {}
        self.cur_image_idx = -1
        self.cur_image = None
        self.elapsed_time = 0

    def get_result(self):
        return self.result

    def get_images(self):
        return self.images

    def get_images_paths(self):
        return self.images_paths

    def get_images_count(self):
        return len(self.get_images_paths())

    def get_elapsed_time(self):
        return self.elapsed_time

    def next_image(self):
        if self.cur_image_idx < len(self.images_paths) - 1:
            self.cur_image_idx += 1
            img = cv2.imread(self.images_paths[self.cur_image_idx])
            self.cur_image = img
            return img
        return None

    def get_cur_image(self):
        return self.cur_image

    def get_cur_image_idx(self):
        return self.cur_image_idx

    def dump_result(self, output_path):
        with open(output_path, 'w+') as f:
            json.dump(self.result, f)

    def load_images(self):
        self.images = []

        for image_path in self.images_paths:
            self.images.append(cv2.imread(image_path))

    def write_result(self, image, algorithm, res, shift_down, is_shift_right):
        is_defected = res['result']
        color = (0, 255, 0) if is_defected == False else (0, 0, 255)
        text = '{}: {}, {}'.format(algorithm, str(is_defected), res['value'])
        self.put_text(image, text, shift_down, is_shift_right, color)

    def put_text(self, image, text, shift_down, is_shift_right, color=(0, 255, 0)):
        (h, w, c) = image.shape
        shift_right = int(0.02 * w) if is_shift_right else 0
        cv2.putText(image, text, (int(0.015 * w) + shift_right, int(0.6 * h + shift_down)), cv2.FONT_HERSHEY_SIMPLEX,
                    int(0.0015 * h),
                    color, 2)

    def add_time(self, time_in_seconds):
        self.elapsed_time += time_in_seconds

    def dump_images(self, debug_path, root_path):
        if debug_path is None:
            return

        # TODO: generalize
        for i, image_path in enumerate(self.images_paths):
            image = cv2.imread(image_path)
            image_name = os.path.relpath(image_path, root_path)

            shift_down_step = 40
            shift_down = 0

            self.put_text(image, 'DEFOCUS:', shift_down, False)
            shift_down += shift_down_step
            defocus_res = self.result[image_path]['DEFOCUS']
            for algorithm, res in defocus_res.iteritems():
                self.write_result(image, algorithm, res, shift_down, True)
                shift_down += shift_down_step

            self.put_text(image, 'OVEREXPOSURE:', shift_down, 0)
            shift_down += shift_down_step
            overexposure_res = self.result[image_path]['OVEREXPOSURE']
            for algorithm, res in overexposure_res.iteritems():
                self.write_result(image, algorithm, res, shift_down, True)
                shift_down += shift_down_step

            self.put_text(image, 'OBSTRUCTION:', shift_down, 0)
            shift_down += shift_down_step
            overexposure_res = self.result[image_path]['OBSTRUCTION']
            for algorithm, res in overexposure_res.iteritems():
                self.write_result(image, algorithm, res, shift_down, True)
                shift_down += shift_down_step

            result_image_path = os.path.join(debug_path, image_name)
            if not os.path.exists(os.path.dirname(result_image_path)):
                os.makedirs(os.path.dirname(result_image_path))
            cv2.imwrite(result_image_path, image)


def init_processors(config):
    return [
        DefocusProcessor(config),
        OverexposureProcessor(config),
        ObstructionProcessor(config)
    ]


def save_result(processor_name, context, algorithm_name, single_img_result, i):
    images_paths = context.get_images_paths()

    image_path = images_paths[i]

    if image_path in context.get_result():
        if processor_name in context.get_result()[image_path]:
            context.get_result()[image_path][processor_name][algorithm_name] = single_img_result
        else:
            context.get_result()[image_path][processor_name] = {
                algorithm_name: single_img_result
            }
    else:
        context.get_result()[image_path] = {
            processor_name: {
                algorithm_name: single_img_result
            }
        }


if __name__ == '__main__':
    ap = argparse.ArgumentParser()

    ap.add_argument("-c", "--config", required=True,
                    help="path to config file")
    ap.add_argument("-o", "--output", required=True,
                    help="path to output file")
    ap.add_argument("-l", '--list', required=True,
                    help='list with paths relative to root to images')
    ap.add_argument("-r", "--root", required=False,
                    help="root path to images")
    ap.add_argument("-d", '--debug', required=False,
                    help='path to debug directory')

    args = vars(ap.parse_args())

    list_path = args['list']
    config_path = args['config']
    output_path = args['output']

    if 'debug' in args:
        debug_path = args['debug']
    else:
        debug_path = None

    if 'root' in args:
        root_path = args['root']
    else:
        root_path = None

    with open(list_path, 'r') as f:
        if root_path is not None:
            images_paths = [os.path.abspath(os.path.join(root_path, img_name.strip())) for img_name in f.readlines()]
        else:
            images_paths = [os.path.abspath(os.path.join(img_name.strip())) for img_name in f.readlines()]

    with open(config_path, 'r') as f:
        config = json.load(f)

    context = ImagesContext(images_paths)

    processors = init_processors(config)

    # start_time = time.time()
    while (True):
        img = context.next_image()
        if img is None:
            break

        for processor in processors:
            res, img_idx = processor.process(context)

            # TODO: Get rid of kostyl
            if res is None:
                continue
            for algorithm_name, single_img_result in res.iteritems():
                save_result(processor.get_processor_name(), context, algorithm_name, single_img_result, img_idx)

    # end_time = time.time()
    # elapsed_time = end_time - start_time
    #
    # print('Total time is: {} seconds, images_count={}, avg_time={}'.format(
    #     elapsed_time, context.get_images_count(), elapsed_time / context.get_images_count()))

    print (
        'Total time is {}, images count is {}, average time is {}'.format(context.get_elapsed_time(),
                                                                          context.get_images_count(),
                                                                          context.get_elapsed_time() / context.get_images_count()))

    context.dump_result(output_path)
    context.dump_images(debug_path, args['root'])
