import argparse
import os
import shutil

if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--images", required=True,
                    help="path to input directory of images")
    ap.add_argument("-o", "--output", required=True, help="path to output images")
    args = vars(ap.parse_args())

    global_images_path = args['images']
    output_path = args["output"]

    for directory in os.listdir(global_images_path):
        images_path = os.path.join(global_images_path, directory + '/' + 'sharp')
        for image_name in os.listdir(images_path):
            image_path = os.path.join(images_path, image_name)
            print('Copy {} to {}'.format(image_path, os.path.join(output_path, directory + '_' + image_name)))
            shutil.copy(image_path, os.path.join(output_path, directory + '_' + image_name))
