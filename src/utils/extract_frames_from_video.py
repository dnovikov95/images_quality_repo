import cv2

vidcap = cv2.VideoCapture('/home/dmitry/Downloads/custom_dataset/24_11/20181124_133131.mp4')
success, image = vidcap.read()
count = 0
while success:
    count += 1
    if (count % 5 == 0):
        cv2.imwrite("/home/dmitry/Downloads/custom_dataset/24_11/overexposure/20181124_133131_%d.jpg" % count,
                    image)  # save frame as JPEG file
    success, image = vidcap.read()
    print('Read a new frame: ', success)
