import argparse
import json
import os

import cv2

from src.core.processors.defocus import DefocusProcessor
from src.core.processors.obstruction import ObstructionProcessor
from src.core.processors.overexposure import OverexposureProcessor


class VideoBasedContext(object):

    def __init__(self, video_file_path):
        self.cap = cv2.VideoCapture(video_file_path)
        self.cur_image = None

    def get_video_cap(self):
        return self.cap

    def relese_video_cap(self):
        self.cap.release()

    def get_images_count(self):
        return 1

    def next_image(self):
        if self.cap.isOpened():
            ret, frame = self.cap.read()
            self.cur_image = frame
            return frame

        return None

    def get_cur_image(self):
        return self.cur_image

    def get_cur_image_idx(self):
        return 0


def init_processors(config):
    return [
        DefocusProcessor(config),
        OverexposureProcessor(config),
        ObstructionProcessor(config)
    ]


if __name__ == '__main__':
    ap = argparse.ArgumentParser()

    ap.add_argument("-c", "--config", required=True, help="path to config file")
    ap.add_argument("-v", "--video", required=True, help="path to video file")
    ap.add_argument("-o", "--output", required=False, help="path to output file")
    ap.add_argument("-d", "--debug", required=False, help="path to output debug directory")

    args = vars(ap.parse_args())

    video_file_path = args['video']
    config_path = args['config']
    output_path = args['output']
    debug_path = args['debug']

    context = VideoBasedContext(video_file_path)
    with open(config_path, 'r') as f:
        config = json.load(f)

    processors = init_processors(config)

    algs_to_use = {
        'DEFOCUS': 'laplacian',
        'OVEREXPOSURE': 'CIELab',
        'OBSTRUCTION': 'hist_based'
    }

    if output_path is not None:
        width = int(context.get_video_cap().get(3))
        height = int(context.get_video_cap().get(4))
        video_writer = cv2.VideoWriter(output_path, cv2.VideoWriter_fourcc(*"H264"), 30, (width, height))

    cur_frame = 0
    while (True):
        img = context.next_image()
        if img is None:
            context.relese_video_cap()
            break

        res = {}
        for processor in processors:
            single_processor_res, img_idx = processor.process(context)
            # TODO: Get rid of kostyl
            if single_processor_res is None:
                continue
            processor_name = processor.get_processor_name()
            res[processor_name] = single_processor_res

        img = context.get_cur_image()

        shift = -100
        shift_step = 100
        (h, w, c) = img.shape

        for processor_name, alg in algs_to_use.iteritems():
            bool_result = res[processor_name][alg]['result']
            value = res[processor_name][alg]['value']
            color = (0, 255, 0) if bool_result == False else (0, 0, 255)
            text = '{}: {}, {}'.format(processor_name, str(bool_result), value)
            cv2.putText(img, text, (int(0.015 * w), int(0.8 * h + shift)), cv2.FONT_HERSHEY_SIMPLEX, int(0.003 * h),
                        color,
                        4)
            shift += shift_step

        if output_path is None:
            cv2.imshow('Frame', img)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                exit(0)
        else:
            video_writer.write(img)

        if debug_path is not None and cur_frame % 10 == 0:
            cv2.imwrite(os.path.join(debug_path, str(cur_frame) + '.jpg'), img)

        cur_frame += 1

    if output_path is not None:
        video_writer.release()

    cv2.destroyAllWindows()
