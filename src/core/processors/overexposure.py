import cv2
import numpy as np

from src.core.processors.abstract import AbstractProcessor


class OverexposureProcessor(AbstractProcessor):

    def __init__(self, config):
        super(OverexposureProcessor, self).__init__(config)
        self.algorithms_impls = {}
        if self.get_config() is not None:
            for alg_name in self.get_config().keys():
                if alg_name.startswith("CIELab"):
                    self.algorithms_impls[alg_name] = self.CIELab_impl
                elif alg_name.startswith("YUV"):
                    self.algorithms_impls[alg_name] = self.YUV_impl
                elif alg_name.startswith("map_based"):
                    self.algorithms_impls[alg_name] = self.map_based_impl

    def get_processor_name(self):
        return 'OVEREXPOSURE'

    def CIELab_impl(self, algorithm_name, image):
        cialab_config = self.get_config()[algorithm_name]

        cielab_img = cv2.cvtColor(image, cv2.COLOR_BGR2Lab)

        L_component = cielab_img[:, :, 0] / 255. * 100

        is_overexposured, overexposed_percent = \
            self.is_overexposured(L_component, cialab_config['threshold'],
                                  cialab_config['percentages_threshold'])

        result = {
            "result": is_overexposured,
            "overexposed_percent": overexposed_percent,
            "value": overexposed_percent
        }

        return result

    def YUV_impl(self, algorithm_name, image):
        yuv_config = self.get_config()[algorithm_name]

        yuv_img = cv2.cvtColor(image, cv2.COLOR_BGR2YUV)

        Y_component = yuv_img[:, :, 0]

        is_overexposured, overexposed_percent = \
            self.is_overexposured(Y_component, yuv_config['threshold'],
                                  yuv_config['percentages_threshold'])

        result = {
            "result": is_overexposured,
            "overexposed_percent": overexposed_percent,
            "value": overexposed_percent
        }

        return result

    def map_based_impl(self, algorithm_name, image):
        cielab_img = cv2.cvtColor(image, cv2.COLOR_BGR2Lab)

        map_config = self.get_config()[algorithm_name]

        L = cielab_img[:, :, 0] / 255. * 100
        a = cielab_img[:, :, 1] - 128.
        b = cielab_img[:, :, 2] - 128.

        overexposure_map = 0.5 * (np.tanh(1 / 60. * ((L - 80.) + 40. - np.sqrt(a * a + b * b))) + 1.)

        is_overexposured, overexposed_percent = \
            self.is_overexposured(overexposure_map, map_config['threshold'],
                                  map_config['percentages_threshold'])

        result = {
            "result": is_overexposured,
            "overexposed_percent": overexposed_percent,
            "value": overexposed_percent
        }

        return result

    def get_overexposed_percent(self, mask):
        overexposed_count = float(np.sum(mask))
        total_count = mask.shape[0] * mask.shape[1]
        return int(overexposed_count * 100 / total_count)

    def get_mask(self, img, threshold):
        vfunc = np.vectorize(lambda x: 1 if x > threshold else 0)
        return vfunc(img)

    def is_overexposured(self, component, mask_threshold, percentages_threshold):
        mask = self.get_mask(component, mask_threshold)

        is_overexposured = False
        overexposed_percent = self.get_overexposed_percent(mask)
        if overexposed_percent > percentages_threshold:
            is_overexposured = True

        return is_overexposured, overexposed_percent
