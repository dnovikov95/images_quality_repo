import json
import os
from shutil import copyfile


def read_file(file_path):
    with open(file_path, 'r') as f:
        res = json.load(f)
    return res


if __name__ == '__main__':
    predicted_file_path = '/home/dmitry/MIPT/Diplom/datasets/overexposure_custom/predicted.json'
    markup_file_path = '/home/dmitry/MIPT/Diplom/datasets/overexposure_custom/markup.json'
    processor_name = 'OVEREXPOSURE'
    method_name = 'CIELab-4'
    output_path = '/home/dmitry/MIPT/Diplom/datasets/overexposure_custom/bad_output'

    predicted = read_file(predicted_file_path)
    markup = read_file(markup_file_path)

    if not os.path.exists(output_path):
        os.makedirs(output_path)

    for key, value in predicted.iteritems():
        predicted_val = value[processor_name][method_name]['result']
        markup_val = markup[key]
        if predicted_val != markup_val:
            copyfile(key, os.path.join(output_path, os.path.basename(key)) + '.' + str(predicted_val) + '.jpg')
