import cv2

from src.core.processors.abstract import AbstractProcessor


class DefocusProcessor(AbstractProcessor):

    def __init__(self, config):
        super(DefocusProcessor, self).__init__(config)
        self.algorithms_impls = {}
        if self.get_config() is not None:
            for alg_name in self.get_config().keys():
                if alg_name.startswith("laplacian"):
                    self.algorithms_impls[alg_name] = self.laplacian_impl

    def get_processor_name(self):
        return 'DEFOCUS'

    def laplacian_impl(self, algorithm_name, image):
        threshold = self.get_config()[algorithm_name]['threshold']

        gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        focus_measure = cv2.Laplacian(gray_image, cv2.CV_64F).var()

        is_defocused = False

        if focus_measure < threshold:
            is_defocused = True

        result = {
            "result": is_defocused,
            "focus_measure": int(focus_measure),
            "value": int(focus_measure)
        }

        return result
