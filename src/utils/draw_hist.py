import os
from matplotlib import pyplot as plt

import cv2

if __name__ == '__main__':
    dataset_dir = '/home/dmitry/MIPT/Diplom/datasets/obscured_custom'
    dst_images_path = '/home/dmitry/MIPT/Diplom/datasets/obscured_custom/histograms'

    with open(os.path.join(dataset_dir, 'images.lst'), 'r') as f:
        images_names = f.readlines()

    for img_name in images_names:
        img = cv2.imread(os.path.join(dataset_dir, img_name).strip(), 0)
        plt.clf()
        plt.hist(img.ravel(), 256, [0, 256])
        plt.savefig(os.path.join(dst_images_path, img_name).strip())
